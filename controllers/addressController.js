const Address = require("../models/address");
const User = require("../models/user");

// add address
module.exports.addAddress = (data) => {

    let newAddress = new Address ({
        userId : data.userId,
        houseNumber : data.address.houseNumber,
        street : data.address.street,
        brgy : data.address.brgy,
        city : data.address.city,
        province : data.address.province,
        zip : data.address.zip,
        country : data.address.country
    })

    return newAddress.save().then((address, error) => {
        if (error) { return false; }

        return address;
    });
}


// get address

module.exports.getAddress = (data) => {
    
        return Address.find({userId : data.userId}).then(result => {
            return result;
        })
    }