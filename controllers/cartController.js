// dependencies
const User = require("../models/user");
const Product = require("../models/product");
const Order = require("../models/order");
const Cart = require("../models/cart");

const bcrypt = require("bcrypt");
const auth = require("../auth");




// add product to cart
module.exports.addToCart = (data) => {

    return Cart.findById(data.userId).then(cart => {
        if(cart){
            let item = cart.items.find(item => item.productId == data.product._id);
            if(item){
                item.quantity += data.product.quantity;
                item.price = data.product.price;
            }else{
                cart.items.push({
                    productId: data.product._id,
                    quantity: data.product.quantity,
                    price: data.product.price
                });
            }
            cart.totalAmount += data.product.price * data.product.quantity;
            return cart.save().then(result => {
                return {
                    message: "Product successfully added to cart",
                    result: result
                }
            })
        }else{
            let cart = new Cart({
                userId: data.userId,
                items: [{
                    productId: data.product._id,
                    quantity: data.product.quantity,
                    price: data.product.price
                }],
                totalAmount: data.product.price * data.product.quantity
            });
            return cart.save().then(result => {
                return {
                    message: "Product successfully added to cart",
                    result: result
                }
            })
        }
    })
}


