// dependencies
const Product = require("../models/product");
const Order = require("../models/order");


// add product to Order
// add product to Order
module.exports.checkOut = async (data,reqParams) => {
  try {
    const product = await Product.findById(reqParams);

    if (!product) {
      return {
        message: "Product not found",
        result: null
      };
    }

    // const isQuantityWholeNumber = Number.isInteger(data.product.quantity);

    // if (!isQuantityWholeNumber) {
    //   return {
    //     message: "Quantity must be a whole number",
    //     result: null
    //   };
    // }

    let order = await Order.findOne({ userId: data.userId });

    console.log(data);

    // productAmount = order.amount + (product.price * data.product.quantity)

    if (order) {
      const existingProduct = order.products.find(
        (p) => p.productId.toString() === reqParams.toString()
      );

      if (existingProduct) {
        existingProduct.quantity += data.product.quantity;
        existingProduct.amount += data.product.quantity * product.price;
      } else {
        order.products.push({
          productId: reqParams,
          product: product.name,
          quantity: data.product.quantity, 
          amount: product.price * data.product.quantity
        });
      }
      order.totalAmount += data.product.quantity * product.price;
    } else {
      order = new Order({
        userId: data.userId,
        name: data.fullName,
        products: [
          {
            productId: reqParams,
            product: product.name,
            quantity: data.product.quantity, 
            amount: product.price * data.product.quantity
          }
        ],
        totalAmount: product.price * data.product.quantity
      });
    }
    const result = await order.save();
    const message = order._id ? "Order successfully updated" : "Order successfully created";
    return {
      message,
      result
    };
  } catch (err) {
    return {
      message: err.message,
      result: null
    };
  }
};





// get all orders (for admin only)
module.exports.getAllOrders = (authentication) => {

  if (authentication.isAdmin) {
    return Order.find().then(result => {
      return result
    }).catch(err => {
      return 'There was an error retrieving all orders.'
    })
  }
  // If user is not an admin, return a message
  return Promise.resolve('User must me Admin to Access this.').then((message) => {
    return message
  })
}


// get user order details (for specific user only) with the use of token
module.exports.getOrderDetails = (authentication) => {

  if (authentication.userId) {
    return Order.find({ userId: authentication.userId }).then(result => {
      return result
    }).catch(err => {
      return 'There was an error retrieving all orders.'
    })
  }
  // If user is not an admin, return a message
  return Promise.resolve('User must me Admin to Access this.').then((message) => {
    return message
  })
}




