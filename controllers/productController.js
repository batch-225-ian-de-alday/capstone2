// Dependencies
const Product = require("../models/product");
/* const Cart = require("../models/cart"); */
/* const auth = require("../auth"); */


// method for creating a new product for admin only
module.exports.createProduct = (data) => {
	if(data.isAdmin){
		// use new because it is creating a new data or products
		let updatedProduct = new Product ({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		})

		return updatedProduct.save().then((updatedProduct, error) => {
			if(error){
				return false
			}

			console.log(`New product successfully created: ${updatedProduct}`)
			return {
				message: 'New product successfully created!',
				result: updatedProduct
			}
		})
	} 

	return Promise.resolve('User must me Admin to Access this.').then((message) => {
		return message
	})
		
}

// Retrieve all products for admin only
module.exports.getAllProducts = (authentication) => {
	// Check if user is an admin
	if(authentication.isAdmin){
		return Product.find().then(result => {
			return result
		}).catch(err => {
			return 'There was an error retrieving all products.'
		})
	}
	// If user is not an admin, return a message
	return Promise.resolve('User must me Admin to Access this.').then((message) => {
		return message
	})
}	
	

// retrieve all active products
module.exports.getActiveProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result
	})
}


// retrieve all archived products for admin only
module.exports.getArchivedProducts = (authentication) => {

	// Check if user is an admin
	if(authentication.isAdmin){
		return Product.find({isActive: false}).then(result => {
			return result
		})
	}
	// If user is not an admin, return a message
	return Promise.resolve('User must me Admin to Access this.').then((message) => {
		return message
	})
}


// retrieve specific product
// used the findbyid method to get the product details
// if the token is an admin, return the product whether it is active or not
// if the token is not an admin, return the product only if it is active
module.exports.getProductDetails = (authentication, reqId) => {

	// Check if user is an admin
	if(authentication.isAdmin){
		return Product.findById(reqId).then(result => {
			return result
		})
	}

	if (!authentication.isAdmin)
	return Product.findById(reqId).then(result => {
		if (result.isActive) {
			return result
	} 
		return {
			message: 'Product not found or not active.'
		}
	})
}

// retrieve specific product with no token
// used the findbyid method to get the product details

module.exports.getProductDetailsNoToken = (reqId) => {
	return Product.findById(reqId).then(result => {
		if (result.isActive) {
			return result
	}
		return {
			message: 'Product not found or not active.'
		}
	})
}



// Update product information
// Use the findbyid and update method to update the product using save()
module.exports.updateProduct = (data, reqParams) => {
	// Check if user is an admin
    if(data.isAdmin){
        return Product.findById(reqParams)
            .then(product => {
                if(product){
                    product.name = data.product.name;
                    product.description = data.product.description;
                    product.price = data.product.price;
                    return product.save();
                } else {
                    return {
                        error: `Product not found with id: ${reqParams}`
                    }
                }
            })
            .then((product) => {
                console.log(`Product updated successfully: ${product}`);
                return {
                    message: "Product updated successfully",
					result: product
                }
            })
            .catch((error) => {
                console.log(`Error updating product: ${error}`);
                return {error: error}
            });
    } 
	// If user is not an admin, return a message
	return Promise.resolve('User must me Admin to Access this.').then((message) => {
		return message
	})
}


// archive product
// Use the findbyidanupdate method to update the product
module.exports.archiveProduct = (authentication, reqParams) => {
	// Check if user is an admin
	if(authentication.isAdmin){
		
		// Set isActive to false
		let deactivateProduct = {
			isActive: false
		}

		// Update product
		return Product.findByIdAndUpdate(reqParams, deactivateProduct, {new: true}).then((product, error) => {
			if (error) {
				console.log(`Error updating product: ${error}`);
				return {error: error}
			} else if(product) {
				console.log(`Product archived successfully: ${product}`);
				return {
					message: "Product archived successfully",
					result: product
				}
			} else {
				return {
					error: `Product not found with id: ${reqParams}`
				}
			}
		})
	}
	// If user is not an admin, return a message
	return Promise.resolve('User must me Admin to Access this.').then((message) => {
		return message
	})
}


// activate product
// Use the findbyidanupdate method to update the product
module.exports.activateProduct = (authentication, reqParams) => {
	// Check if user is an admin
	if(authentication.isAdmin){
		
		// Set isActive to true
		let activateProduct = {
			isActive: true
		}
		
		// Update product
		return Product.findByIdAndUpdate(reqParams, activateProduct, {new: true}).then((product, error) => {
			if (error) {
				console.log(`Error updating product: ${error}`);
				return {error: error}
			} else if(product) {
				console.log(`Product activated successfully: ${product}`);
				return {
					message: "Product activated successfully",
					result: product
				}
			} else {
				return {
					error: `Product not found with id: ${reqParams}`
				}
			}
		})
	}
	// If user is not an admin, return a message
	return Promise.resolve('User must me Admin to Access this.').then((message) => {
		return message
	})
}


/* // get product price by id
module.exports.getProductPrice = (reqParams) =>
	Product.findById(reqParams).then(result => {
		return result
	})
 */

	
	



