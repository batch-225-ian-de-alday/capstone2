// dependencies
const User = require("../models/user");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// check email
module.exports.checkEmail = (reqBody) => {

    return User.find({email : reqBody.email}).then(result => {

        if (result.length > 0) {
            return true
        } else {
            return false
        }
    })
}

// User registration
module.exports.registerUser = (reqBody) => {

    let newUser = new User ({
        firstName : reqBody.firstName,
        lastName : reqBody.lastName,
        email : reqBody.email,
        password : bcrypt.hashSync(reqBody.password, 10)
    });

    return newUser.save().then((user, error) => {
        if (error) { 
            return false; 
        }
        return user;
    }); 
}

// User registration
// module.exports.registerUser = (reqBody) => {

//     let newUser = new User ({
//         firstName : reqBody.firstName,
//         lastName : reqBody.lastName,
//         email : reqBody.email,
//         password : bcrypt.hashSync(reqBody.password,10)
//     })


//     // condition to check if the email is already registered
//     let saveUserResult = User.find({email : reqBody.email}).then(result => {

//         return Boolean(result.length)

//     }).then(isEmailExist => {

//         if (!isEmailExist) {

//             return newUser.save().then((user, error) => {
//                 if (error) { return false; }

//                 return user;
//             });
//         } 

//         return 'Email is already exists.'
//     })

//     return saveUserResult       
// }

// User login
module.exports.loginUser = (reqBody) => {

    return User.findOne({email :reqBody.email}).then(result => {

        if (result == null) {
            
            return {
                message: "Email not found in our database"
            }

        } else {

            // The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password) 

            if (isPasswordCorrect) {

                return {access : auth.createAccessToken(result)}

            } else {

                // if password doesn't match
                return {
                    message: "Password was incorrect"
                }
            };
        };
    });
}

// User update as admin
// Use the findbyidandupdate method to update the user
module.exports.updateUser = (authentication, reqParams) => {
    if(authentication.isAdmin){
        // no need for new since it is updating an existing data or Users
        let userAdmin = {
            isAdmin: true            
        }

        return User.findByIdAndUpdate(reqParams, userAdmin, {new: true}).then((user, error) => {
            if (error) { 
                console.log(`Error updating user: ${error}`);
                return {error: error}
            } else if(user) {
                console.log(`User updated successfully: ${user}`);
                return {
                    message: "User updated successfully",
                    result: user
                }
            } else {
                console.log(`User not found with id: ${reqParams}`);
                return {
                    error: `User not found with id: ${reqParams}`
                }
            }
        });
    }
    // If user is not an admin, return a message
	return Promise.resolve('User must me Admin to Access this.').then((message) => {
		return message
	})
}


// get all users as admin
module.exports.getAllUsers = (authentication) => {

    if (authentication.isAdmin) {

        return User.find().then(result => {

            return result

        })

    }
    return Promise.resolve('User must me Admin to Access this.').then((message) => {
		return message
	})
}

// get user details as admin
// Use the findbyid method to get the user details
// findbyid is partner to :id in the route 
module.exports.getUserDetails = (authentication, userId) => {
    if (authentication.isAdmin) {
        return User.findById(userId).then(result => {
            return result
        })
    }
    return Promise.resolve('User must me Admin to Access this.').then((message) => {
		return message
	})
}

// Route for retrieving user details
module.exports.getProfile = (userData) => {
    return User.findById(userData).then(result => {
        if (result == null) {
            return false
        } else {
            result.password = ""
            return result
        }
    });
};


    


