const mongoose = require("mongoose");

const addressSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: [true, "User ID is required"]
    },
    houseNumber: {
        type: String
    },
    street: {
        type: String,
        required: [true, "Street is required"]
    },
    brgy: {
        type: String,
        required: [true, "Brgy is required"]
    },
    city: {
        type: String,
        required: [true, "City is required"]
    },
    province: {
        type: String,
        required: [true, "Province is required"]
    },
    zip: {
        type: Number,
        required: [true, "Zip is required"]
    },
    country: {
        type: String,
        required: [true, "Country is required"]
    },
    isDefault: {
        type: Boolean,
        default: false
    }
});

module.exports = mongoose.model("Address", addressSchema);
