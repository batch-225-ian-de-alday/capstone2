//dependencies
const mongoose = require("mongoose");


// Cart Schema  
const cartSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: [true, "User ID is required"]
    },
    items: [
        {
            productId: {
                type: mongoose.Schema.Types.ObjectId,
                ref: "Product",
                required: [true, "Product ID is required"]
            },
            quantity: {
                type: Number,
                required: [true, "Quantity is required"]
            },

            price: {
                type: Number,
                required: [true, "Price is required"]
            }
        }
    ],
    totalAmount: {
        type: Number,
        default: 0
    }
});




module.exports = mongoose.model("Cart", cartSchema)