//dependencies
const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema ({
    
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: [true, "User ID is required"]
    },

    name: {
        type: String,
        ref: "User",
        required: [true, "Name is required"]
    },

    purchasedOn: {
        type: Date,
        default: new Date()
    },

    totalAmount: {
        type: Number,
        required: [true, "Total Amount is required"]
    },

    products: [
        {
            productId: {
                type: mongoose.Schema.Types.ObjectId,  
                ref: "Product",
                required: [true, "Product ID is required"]
            },

            product: {
                type: String,
                ref: "Product",
                required: [true, "Product is required"]
            },

            quantity: {
                type: Number,
                default: 1,
                required: [true, "Quantity is required"]
            },
            amount: {
                type: Number,
                ref: "Product",
                required: [true, "Amount is required"]
            }
        }
    ]
});


module.exports = mongoose.model("Order", orderSchema);