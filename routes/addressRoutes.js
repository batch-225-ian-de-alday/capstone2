const express = require("express");
const router = express.Router();
const auth = require("../auth");
const addressController = require("../controllers/addressController");

// user add address routes
router.post("/addAddress", auth.verify, (req,res) => {
    
        const data = {
            address: req.body,
            isAdmin: auth.decode(req.headers.authorization).isAdmin,
            userId: auth.decode(req.headers.authorization).id
        }
    
        addressController.addAddress(data).then(result => res.send(result))
    });



// user get address routes
router.get("/getAddress", auth.verify, (req,res) => {

    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        userId: auth.decode(req.headers.authorization).id
    }

    addressController.getAddress(data).then(result => res.send(result))
});




module.exports = router;