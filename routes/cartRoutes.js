// dependencies
const express = require("express");
const router = express.Router();
const auth = require("../auth");
const cartController = require("../controllers/cartController");
const productController = require("../controllers/productController");
const userController = require("../controllers/userController");
const product = require("../models/product");
const Product = require("../models/product");


// user add to cart routes
router.post("/add", auth.verify, (req,res) => {

    const data = {
        product: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        userId: auth.decode(req.headers.authorization).id
    }

    cartController.addToCart(data).then(result => res.send(result))
});


module.exports = router;