// dependencies
const express = require("express");
const router = express.Router();
const auth = require("../auth");
const orderController = require("../controllers/orderController");


// user check out products
router.post("/checkout/:id", auth.verify, (req,res) => {

    const data = {
        product: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        userId: auth.decode(req.headers.authorization).id,
        fullName: auth.decode(req.headers.authorization).fullName
    }

    orderController.checkOut(data, req.params.id).then(result => res.send(result))
});


// get all orders (for admin only)
router.get("/all", auth.verify, (req,res) => {

    const authentication = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
    }

    orderController.getAllOrders(authentication).then(result => res.send(result))
})


// get user order details (for specific user only) with the use of token
router.get("/details", auth.verify, (req,res) => {
    
    const authentication = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        userId: auth.decode(req.headers.authorization).id
    }

    orderController.getOrderDetails(authentication).then(result => res.send(result))
})




module.exports = router;