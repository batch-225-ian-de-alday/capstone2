//dependencies
const express = require("express");
const router = express.Router();
const auth = require("../auth");
const productController = require("../controllers/productController");



// Product Creation Routes
router.post("/create", auth.verify, (req,res) => {

    const data = {
        product: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
    }

    productController.createProduct(data).then(result => res.send(result))
});


// retrieve all products routes for admin only
router.get("/all", auth.verify, (req,res) => {

    const authentication = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
    }

    productController.getAllProducts(authentication).then(result => res.send(result))
})


// retrieve all active products routes
router.get("/active", (req,res) => {
    productController.getActiveProducts().then(result => res.send(result))
})


// retrieve all archived products routes for admin only
router.get("/archived", auth.verify, (req,res) => {

    const authentication = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
    }

    productController.getArchivedProducts(authentication).then(result => res.send(result))
})


// retrieve specifc product routes for admin only
// if the token is an admin, return the product whether it is active or not
// if the token is not an admin, return the product only if it is active
router.get("/details/:id", auth.verify, (req,res) => {

    const authentication = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
    }

    productController.getProductDetails(authentication, req.params.id).then(result => res.send(result))
})


//retrieve specific product routes for no token required
router.get("/details/no-token/:id", (req,res) => {
    productController.getProductDetailsNoToken(req.params.id).then(result => res.send(result))
})



// update product information routes
router.put("/update/:id", auth.verify, (req,res) => {

    const data = {
        product: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
    }

    productController.updateProduct(data, req.params.id).then(result => res.send(result))
})


// archive product routes
router.patch("/archive/:id", auth.verify, (req,res) => {

    const authentication = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
    }

    productController.archiveProduct(authentication, req.params.id).then(result => res.send(result))
})


// activate product routes
router.patch("/activate/:id", auth.verify, (req,res) => {
    
    const authentication = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
    }
    
    productController.activateProduct(authentication, req.params.id).then(result => res.send(result))
})






// for exporting the product routes
module.exports = router;