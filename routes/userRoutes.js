//dependencies
const express = require("express");
const router = express.Router();
const auth = require("../auth");
const userController = require("../controllers/userController");

// check email 
router.post("/checkEmail", (req,res) => {
    userController.checkEmail(req.body).then(result => res.send(result))
})


// User registration Routes 
router.post("/register", (req,res) => {
    userController.registerUser(req.body).then(result => res.send(result))
})

// User login Routes
router.post("/login", (req,res) => {
    userController.loginUser(req.body).then(result => res.send(result))
})

// User update Routes as admin (for admin only)
router.patch("/update/:id", auth.verify, (req,res) => {

    const authentication = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

    userController.updateUser(authentication, req.params.id).then(result => res.send(result));
});


// get all users (for admin only)
router.get("/all", auth.verify, (req,res) => {

    const authentication = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
    }
    userController.getAllUsers(authentication).then(result => res.send(result))
})


// get specific user details (for admin only)
// router.get("/details/:id", auth.verify, (req,res) => {

//     const authentication = {
//         isAdmin: auth.decode(req.headers.authorization).isAdmin,
//     }
//     userController.getUserDetails(authentication, req.params.id).then(result => res.send(result))
// })

// Route for retrieving user details
router.get("/details", auth.verify, (req,res) => {
    const userData = auth.decode(req.headers.authorization);
    userController.getProfile({_id : userData.id}).then(result => res.send(result))
})


module.exports = router;
