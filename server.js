// Dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const dotenv = require("dotenv").config();
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");
const addresRoutes = require("./routes/addressRoutes");


// Server and Port
const app = express();
const port = 3001;

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));


//Mongoose Connection
mongoose.connect(`mongodb+srv://${process.env.MONGODBUSER}:${process.env.PASSWORD}@cluster0.qmugmsf.mongodb.net/capstone-2-api?retryWrites=true&w=majority`,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }

);


// Setup notification for connection success or failure
let db = mongoose.connection;

db.on("error", console.error.bind(console, "Failed to connect MongoDB"));
db.on("open", () => console.log("Connected to MongoDB"));


// address for Routes
app.use("/users", userRoutes, addresRoutes);
app.use("/products", productRoutes);
// app.use("/carts", cartRoutes);
app.use("/orders", orderRoutes)



app.listen(port, () => console.log(`API is now online on port ${port}`));
